package pl.oakfusion.forge.i18n;

import java.util.*;

public class MessageSource {

	private ResourceBundle resourceBundle;
	private MultiResourceBundleControl multiResources;

	public MessageSource(String bundleName, String... bundles) {
		multiResources = new MultiResourceBundleControl(bundleName, bundles);
		resourceBundle = ResourceBundle.getBundle(multiResources.getBaseName(), Locale.getDefault(), multiResources);
	}

	public void setLanguage(String language) {
		ResourceBundle.clearCache();
		resourceBundle = ResourceBundle.getBundle(multiResources.getBaseName(), Locale.forLanguageTag(language), multiResources);
	}

	public String getString(String messageKey) {
		return resourceBundle.getString(messageKey);
	}
}
