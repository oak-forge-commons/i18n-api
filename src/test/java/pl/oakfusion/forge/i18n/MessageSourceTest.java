package pl.oakfusion.forge.i18n;

import org.junit.jupiter.api.*;

import static org.assertj.core.api.Assertions.*;

public class MessageSourceTest {

	@Test
	public void should_get_String_from_properties_single_input_default() {
		//given
		MessageSource messageSource = new MessageSource("cos", "test");

		//when
		String output = messageSource.getString("hello");
		//then
		assertThat(output).isEqualTo("Thy shall be  greeted  in my domain");
	}

	@Test
	public void should_get_String_from_properties_single_input_local() {
		//given

		MessageSource messageSource = new MessageSource("pl", "test");

		//when
		String output = messageSource.getString("name");
		//then
		assertThat(output).isEqualTo("Game master");
	}
}
